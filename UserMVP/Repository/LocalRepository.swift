//
//  LocalRepository.swift
//  UserMVP
//
//  Created by Alin Stanusescu on 27.10.2021.
//

import Foundation
import RealmSwift

class LocalRepository  {
 
//    let realm = try! Realm()
//    
////    private func saveUser(userModel: User) {
////
////        try! realm.write {
////            realm.add(userModel, update: true)
////        }
////    }
//    
//    private func write(userModel: User) {
//        let table = Furniture.create(withName: "table")
//        let chair = Furniture.create(withName: "chair")
//        let store = Store.create(withName: "Test Store",
//                                 furniture: [table, chair])
//   
//        // Write to Realm
//        print("Write to Realm")
//        try! realm.write {
//            realm.add(userModel)
//        }
//    }
//    
//    private func read() {
//        // Read from Realm
//        print("Read from Realm")
//        let data = realm.objects(User.self)
//        print(data)
//    }
//    
//    private func update() {
//        // Update data
//        if let table = realm.objects(User.self).first {
//            try! realm.write {
//                table.name = "New Table Name"
//            }
//
//            print(realm.objects(User.self).first)
//        }
//    }
//    
//    private func delete() {
//        // Delete data
//        print("Delete Data")
//        if let tableToDelete = realm.objects(Furniture.self).first {
//            try! realm.write {
//                realm.delete(tableToDelete)
//            }
//
//            print(realm.objects(Furniture.self).first)
//            print(realm.objects(Store.self).first)
//        }
//    }
}

//class Furniture: Object {
//    @objc dynamic var name = ""
//    
//    static func create(withName name: String) -> Furniture {
//        let furniture = Furniture()
//        furniture.name = name
//        
//        return furniture
//    }
//}
//
//class Store: Object {
//    @objc dynamic var name = ""
//    var furniture = List<Furniture>()
//    
//    static func create(withName name: String,
//                       furniture: [Furniture]) -> Store {
//        let store = Store()
//        store.name = name
//        store.furniture.append(objectsIn: furniture)
//        
//        return store
//    }
//}
