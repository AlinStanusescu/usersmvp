//
//  ApiRouter.swift
//  UserMVP
//
//  Created by Alin Stanusescu on 27.10.2021.
//

import Foundation

enum APIRouter  {

    case fetchRandomUsers(pageResultstNo: Int)
    
    // MARK: - Path
    private var path: String {
        switch self {
        case .fetchRandomUsers(let pageResultstNo):
            return "/api/?results=\(pageResultstNo)"
        }
    }
    
    // MARK: - URL
    func asURLRequest() ->  URL {
        let url : URL?
        
        switch self {
        case .fetchRandomUsers(_):
            url =  URL.init(string: Constants.ProductionServer.baseURL + self.path)!
        }
        
        return url!
    }
}

// MARK: -
func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

// MARK: -
extension URLSession {
    fileprivate func codableTask<T: Codable>(with url: URL, completionHandler: @escaping (T?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
                completionHandler(nil, response, error)
                return
            }
            completionHandler(try? newJSONDecoder().decode(T.self, from: data), response, nil)
        }
    }

    func userTask(with url: URL, completionHandler: @escaping (User?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.codableTask(with: url, completionHandler: completionHandler)
    }
}
