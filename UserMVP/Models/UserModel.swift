//
//  UserModel.swift
//  UserMVP
//
//  Created by Alin Stanusescu on 27.10.2021.
//

import Foundation

class User: NSObject, Codable {
    let results: [Result]?
    let info: Info?
}

class Info: NSObject, Codable {
    let seed: String?
    let results, page: Int?
    let version: String?
}

class Result: NSObject, Codable {
    let gender: Gender?
    let name: Name?
    let location: Location?
    let email: String?
    let login: Login?
    let dob, registered: Dob?
    let phone, cell: String?
    let id: ID?
    let picture: Picture?
    let nat: String?
}

class Dob: NSObject, Codable {
    let date: String?
    let age: Int?
}

enum Gender: String, Codable {
    case female = "female"
    case male = "male"
}

class ID: NSObject, Codable {
    let name: String?
    let value: String?

    init(name: String?, value: String?) {
        self.name = name
        self.value = value
    }
}

class Location: NSObject, Codable {
    let street: Street?
    let city, state, country: String?
//    let postcode: String?
    let coordinates: Coordinates?
    let timezone: Timezone?
}

class Coordinates: NSObject, Codable {
    let latitude, longitude: String?
}

class Street: NSObject, Codable {
    let number: Int?
    let name: String?
}

class Timezone: NSObject, Codable {
    let offset, timezoneDescription: String?

    enum CodingKeys: String, CodingKey {
        case offset
        case timezoneDescription = "description"
    }
}

class Login: NSObject, Codable {
    let uuid, username, password, salt: String?
    let md5, sha1, sha256: String?
}

class Name: NSObject, Codable {
    let title, first, last: String?
}

class Picture: NSObject, Codable {
    let large, medium, thumbnail: String?
}
