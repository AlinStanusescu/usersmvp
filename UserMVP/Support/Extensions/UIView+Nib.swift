//
//  UIView+Nib.swift
//  -
//
//  Created by Alin Stanusescu on 04/06/2018.
//  Copyright © 2018 Orange. All rights reserved.
//

import UIKit

extension UIView {
    
    @discardableResult
    func loadNib<T : UIView>() -> T? {
        guard let view = Bundle.main.loadNibNamed(String(describing: type(of: self)), owner: self, options: nil)?[0] as? T else {
            return nil
        }
        self.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addAllMarginConstraints()
        return view
    }
    
    @discardableResult
    func loadNib<T : UIView>(withName name: String) -> T? {
        guard let view = Bundle.main.loadNibNamed(name, owner: self, options: nil)?[0] as? T else {
            return nil
        }
        self.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addAllMarginConstraints()
        return view
    }
}

