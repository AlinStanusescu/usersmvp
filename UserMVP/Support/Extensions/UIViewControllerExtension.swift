//
//  UIViewControllerExtension.swift
//  UserMVP
//
//  Created by Alin Stanusescu on 27.10.2021.
//

import UIKit

protocol NibLoaded {}

extension UIViewController: NibLoaded {}

extension NibLoaded where Self: UIViewController {
    static func create() -> Self {
        let nibName = "\(self)".split { $0 == "." }.map(String.init).last!
        return self.init(nibName: nibName, bundle: nil)
    }
}
