//
//  BaseViewController.swift
//  UserMVP
//
//  Created by Alin Stanusescu on 27.10.2021.
//

import UIKit
import JGProgressHUD

class BaseViewController: UIViewController {

    func backAction() {
        UserControlFlow.shared.backAction(parentController: self)
    }
    
    // Init and present loading view
    func presentLoadingActivityView(hud: JGProgressHUD) {
        hud.style = .dark
        hud.textLabel.text = "Loading..."
        hud.show(in: self.view)
    }
    
    func removeLoadingActivityView(hud: JGProgressHUD) {
        hud.dismiss(afterDelay: 0.3)
    }
    
    func presetErrorMessageAlert() {
        let hud = JGProgressHUD.init(style: .dark)
        hud.textLabel.text = "An error occurred!"
        hud.detailTextLabel.text = "We display the last available data set."
        hud.indicatorView = JGProgressHUDErrorIndicatorView.init()

        hud.show(in: self.view)
        hud.dismiss(afterDelay: 3.0)
    }
}
