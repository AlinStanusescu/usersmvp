//
//  UserTableViewCell.swift
//  UserMVP
//
//  Created by Alin Stanusescu on 16.12.2021.
//

import UIKit

class UserTableViewCell: UITableViewCell {

    static let identifier = "UserTableViewCell"
    
    @IBOutlet weak var cardImageView: UIImageView!
    @IBOutlet weak var cardName: UILabel!
    @IBOutlet weak var cardAdress: UILabel!
    @IBOutlet weak var cardNationality: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }

    private func setup() {
        self.selectionStyle = .none
        
        cardName.font = UIFont.systemFont(ofSize: 15)
        cardName.textColor = Colors.darkGrey3
        
        cardAdress.font = UIFont.systemFont(ofSize: 13)
        cardAdress.textColor = Colors.textColor_MildGrey1
                
        cardNationality.font =  UIFont.systemFont(ofSize: 13)
        cardNationality.textColor = Colors.textColor_MildGrey2
        
        cardImageView.layer.borderWidth = 1
        cardImageView.layer.borderColor = UIColor.darkGray.cgColor
    }
    
    var viewModel: UserTableViewCellModel? {
        didSet { self.reloadView() }
    }
    
    func reloadView() {
        guard let model = self.viewModel else { return }
        
        if self.cardImageView.image?.ciImage == nil && self.cardImageView.image?.cgImage == nil {
            let url = URL(string: model.userPreviewImage)
            DispatchQueue.main.async {
                let data = try? Data(contentsOf: url!)
                self.cardImageView.image = UIImage(data: data!)
            }
        }

        cardName.text =  model.userName
        cardAdress.text = model.userState
        cardNationality.text = model.userNationality
    }
}

