//
//  UserTableViewCellModel.swift
//  UserMVP
//
//  Created by Alin Stanusescu on 16.12.2021.
//

import Foundation

protocol UserTableViewCellModel {
    var userName: String { get }
    var userPreviewImage: String { get }
    var userNationality: String { get }
    var userState: String { get }
}

extension Result: UserTableViewCellModel {
    var userName: String { return "\(self.name?.title ??  ""). \(self.name?.first  ?? "") \(self.name?.last ?? "")" }
    var userPreviewImage: String {  return self.picture?.thumbnail ?? "" }
    var userNationality: String {  return  self.nat  ?? "" }
    var userState: String {  return "\(self.location?.state ?? "") | \(self.location?.country ?? "") | \(self.location?.city ?? "")" }
}
