//
//  UserDetailRowView.swift
//  UserMVP
//
//  Created by Alin Stanusescu on 28.10.2021.
//

import UIKit

class UserDetailRowView: NibLoadingView {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    var model: UserDetailRowModel? {
        didSet {
            self.reloadView()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView = legacyLoadXib()
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        contentView = legacyLoadXib()
        setup()
    }
               
    override func setup() {
        super.setup()
        self.titleLabel.font = UIFont.systemFont(ofSize: 13)
        self.titleLabel.textColor = Colors.darkGrey3
    }
    
    func reloadView() {
        guard let model = model else {
            return
        }

        self.titleLabel.text = model.title
    }
}
