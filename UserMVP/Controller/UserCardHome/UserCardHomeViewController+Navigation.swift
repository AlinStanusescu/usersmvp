//
//  UserCardHomeViewController+Navigation.swift
//  UserMVP
//
//  Created by Alin Stanusescu on 28.10.2021.
//

import UIKit

extension UserCardHomeViewController {
    
    func presentUserCardDetailsScreen(model: Result) {
        UserControlFlow.shared.presentUserCardDetails(self, model: model)
    }
}
