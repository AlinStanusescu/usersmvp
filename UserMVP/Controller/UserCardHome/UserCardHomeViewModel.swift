//
//  UserCardHomeViewModel.swift
//  UserMVP
//
//  Created by Alin Stanusescu on 27.10.2021.
//

import Foundation

class UserCardHomeViewModel: NSObject {
    
    var userModel: User?
    private(set) var datasource = [Result]()
    
    func fetchUsers(successHandler: @escaping (Bool?) -> Void,
                    errorHandler: @escaping (Error) -> Void) {
        
        let group = DispatchGroup()
        var resultErrors = [Error]()
        
        group.enter()
        APIClient.fetchRandomUsers(pageResultstNo: Constants.noOfRequestedUsers) { result in
            guard let user = result else {
                group.leave()
                return
            }
            
            group.enter()
            self.getDatasource(user) { status in
                self.persistUserData(user)
                group.leave()
            }
            
            group.leave()
        }  errorHandler: { error in
            self.fetchUserDataFromLocal()
            resultErrors.append(error)
            group.leave()
        }
        
        group.notify(queue: .main) { [weak self] in
            if resultErrors.isEmpty == false, let handledError = resultErrors.first {
                errorHandler(handledError)
            } else {
                successHandler(true)
            }
        }
    }
    
    private func persistUserData(_ user: User) {
        UDManagerClient.removeUserModel()
        UDManagerClient.saveUserModel(user: user)
    }
    
    private func fetchUserDataFromLocal() {
        UDManagerClient.fetchUserModel()
        self.datasource.removeAll()
        
        guard let user = UDManagerClient.userModel,
              let users = user.results  else {
            return
        }
        
        for model in users {
            self.datasource.append(model)
        }
    }
    
    private func getDatasource(_ user: User,
                               successHandler: @escaping (Bool?) -> Void) {
        guard let users = user.results else {
            successHandler(false)
            return
        }
        
        for model in users {
            self.datasource.append(model)
        }
        successHandler(true)
    }
}
