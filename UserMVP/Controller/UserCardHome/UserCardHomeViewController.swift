//
//  UserCardHomeViewController.swift
//  UserMVP
//
//  Created by Alin Stanusescu on 27.10.2021.
//

import UIKit
import JGProgressHUD

class UserCardHomeViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    var viewModel = UserCardHomeViewModel()
    let loadingAnimationDuration: TimeInterval = 0.3
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        self.setupTableView()
        self.loadData(firstLoad: true)
    }
    
    private func setup() {
        self.titleLabel.font = UIFont.boldSystemFont(ofSize: 17)
        self.titleLabel.textColor = Colors.darkGrey3
        self.titleLabel.text = "List of users from different regions / areas of the world"

        self.subtitleLabel.font = UIFont.systemFont(ofSize: 13)
        self.subtitleLabel.textColor = Colors.textColor_MildGrey2
        self.subtitleLabel.text = "*for more details tap on a user"
    }
    
    private func loadData(firstLoad: Bool = false) {
        let hud = JGProgressHUD()
        self.presentLoadingActivityView(hud: hud)
        
        self.viewModel.fetchUsers{ [weak self] result in
            guard let self = self else { return }
            DispatchQueue.main.async {
                self.removeLoadingActivityView(hud: hud)
                if result != nil {
                    self.tableView.reloadData()
                    if firstLoad {
                        self.animateInLoadedData()
                    }
                }
            }
        } errorHandler: { error in
            self.removeLoadingActivityView(hud: hud)
            self.presetErrorMessageAlert()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
                self?.loadData(firstLoad: true)
            }
        }
    }
 
    private func animateInLoadedData() {
        self.tableView.alpha = 0
        self.tableView.isHidden = false
        
        UIView.animate(withDuration: loadingAnimationDuration) {
            self.tableView.alpha = 1
        }
    }
    
    // MARK:-
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let  bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height
        if (bottomEdge >= scrollView.contentSize.height) {
            self.loadData()
        }
    }
}

extension UserCardHomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    fileprivate func setupTableView() {
        self.tableView.register(UINib.init(nibName: "UserTableViewCell", bundle: nil), forCellReuseIdentifier: UserTableViewCell.identifier)
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    func numberOfSections(in tableView: UITableView) -> Int { return 1 }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.viewModel.datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: UserTableViewCell.identifier) as? UserTableViewCell {
            let model = self.viewModel.datasource[indexPath.row]
            cell.viewModel = model
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = self.viewModel.datasource[indexPath.row]
        self.presentUserCardDetailsScreen(model: model)
    }
}
