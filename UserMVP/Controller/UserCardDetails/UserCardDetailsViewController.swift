//
//  UserCardDetailsViewController.swift
//  UserMVP
//
//  Created by Alin Stanusescu on 28.10.2021.
//

import UIKit

class UserCardDetailsViewController: BaseViewController {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var stateRow: UserDetailRowView!
    @IBOutlet weak var addressRow: UserDetailRowView!
    @IBOutlet weak var timezoneRow: UserDetailRowView!
    @IBOutlet weak var nationalityRow: UserDetailRowView!
    @IBOutlet weak var emailRow: UserDetailRowView!
    @IBOutlet weak var loginRow: UserDetailRowView!
    
    var viewModel: UserCardDetailsModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        self.reloadView()
    }
    
    private func setup() {
        self.titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
        self.titleLabel.textColor = Colors.darkGrey3
        self.titleLabel.text = "User details:  \(self.viewModel.userName)"
    }
    
    func reloadView() {
        guard let viewModel = self.viewModel else {
            return
        }

        stateRow.model = UserDetailRowModel.init(title: viewModel.state)
        addressRow.model = UserDetailRowModel.init(title: viewModel.address)
        timezoneRow.model = UserDetailRowModel.init(title: viewModel.timezone)
        nationalityRow.model = UserDetailRowModel.init(title: viewModel.nationality)
        emailRow.model = UserDetailRowModel.init(title: viewModel.email)
        loginRow.model = UserDetailRowModel.init(title: viewModel.loginDetails)
    }
    
    @IBAction func backButtonTapped(_ : Any?) {
        UserControlFlow.shared.backAction(parentController: self)
    }
}
