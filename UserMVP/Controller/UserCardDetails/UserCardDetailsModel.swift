//
//  UserCardDetailsModel.swift
//  UserMVP
//
//  Created by Alin Stanusescu on 28.10.2021.
//

import Foundation

class UserCardDetailsModel: NSObject {
    
    var model: Result
    
    init(model: Result) {
        self.model = model
    }
    
    var userName: String {
        return "\(model.name?.title ??  ""). \(model.name?.first  ?? "") \(model.name?.last ?? "")"
    }

    var state: String {
        return "State: \(model.location?.state ?? ""), \(model.location?.country ?? ""), \(model.location?.city ?? "")"
    }
    
    var address: String {
        return "Address: \(model.location?.street?.number ?? 0), \(model.location?.street?.name ?? "")"
    }

    var timezone: String  {
        return "Timezone: \(model.location?.timezone?.offset ?? ""), \(model.location?.timezone?.timezoneDescription ?? "")"
    }
    
    var nationality: String  {
        return "Nationality: \(model.nat ?? "")"
    }
    
    var email: String {
        return "Email: \(model.email ?? "")"
    }
    
    var loginDetails: String {
        return "Username: \(model.login?.username ?? "")\nPassword: \(model.login?.password ?? "")\nUUID: \(model.login?.uuid ?? "") "
    }
}
